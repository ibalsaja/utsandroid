package lana.ahmad.myapplication.uts

import android.app.AlertDialog
import android.content.ContentValues
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.CursorAdapter
import android.widget.ListAdapter
import android.widget.SimpleCursorAdapter
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_data_kategori.view.*


class FragmentKategori : Fragment(), View.OnClickListener {

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnTambahKtg->{
                builder.setTitle("Konfirmasi").setMessage("Data yang dimasukkan sudah benar?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnTambahKtgDialog)
                    .setNegativeButton("Tidak",null)
                builder.show()
            }
            R.id.btnEditKtg->{
                builder.setTitle("Konfirmasi").setMessage("Data yang diubah sudah benar?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnEditKtgDialog)
                    .setNegativeButton("Tidak",null)
                builder.show()
            }
            R.id.btnHapusKtg->{
                builder.setTitle("Konfirmasi").setMessage("Yakin ingin menghapus data ini?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnHapusKtgDialog)
                    .setNegativeButton("Tidak",null)
                builder.show()
            }
        }
    }
    lateinit var  thisParent : MainActivity
    lateinit var db:SQLiteDatabase
    lateinit var adapter : ListAdapter
    lateinit var v :View
    lateinit var builder : AlertDialog.Builder
    var idKategori : String = ""
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        thisParent = activity as MainActivity
        db = thisParent.getDbObject()
        v = inflater.inflate(R.layout.frag_data_kategori,container,false)
        v.btnEditKtg.setOnClickListener(this)
        v.btnTambahKtg.setOnClickListener(this)
        v.btnHapusKtg.setOnClickListener(this)
        builder = AlertDialog.Builder(thisParent)
        v.lsKtg.setOnItemClickListener(itemClick)
        return v
    }
    fun showDataKategori(){
        val cursor : Cursor = db.query("kategori", arrayOf("nama_kategori","id_kategori as _id"),
        null,null,null,null,"nama_kategori asc")
        adapter = SimpleCursorAdapter(thisParent,R.layout.item_data_kategori,cursor, arrayOf("_id","nama_kategori"),
            intArrayOf(R.id.txIdKtg,R.id.txKtg),CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        v.lsKtg.adapter = adapter
    }

    override fun onStart() {
        super.onStart()
        showDataKategori()
    }
    val itemClick = AdapterView.OnItemClickListener{ parent, view, position, id->
        val c:Cursor=parent.adapter.getItem(position)as Cursor
        idKategori=c.getString(c.getColumnIndex("_id"))
        v.edMerk.setText(c.getString(c.getColumnIndex("nama_kategori")))
    }
    fun insertDataKategori(namaKategori: String){
        var cv : ContentValues = ContentValues()
        cv.put("nama_kategori",namaKategori)
        db.insert("kategori",null,cv)
        showDataKategori()
    }
    fun updateDataKatgori(namaKategori: String, idBarang: String){
        var cv : ContentValues = ContentValues()
        cv.put("nama_kategori",namaKategori)
        db.update("kategori",cv,"id_kategori=$idKategori",null)
        showDataKategori()
    }
    fun deleteDataKategori(idKategori: String){
        db.delete("kategori","id_kategori=$idKategori",null)
        showDataKategori()
    }
    val btnTambahKtgDialog = DialogInterface.OnClickListener{ dialog, which->
        insertDataKategori(v.edMerk.text.toString())
        v.edMerk.setText("")
    }
    val btnEditKtgDialog = DialogInterface.OnClickListener {dialog,which->
        updateDataKatgori(v.edMerk.text.toString(),idKategori)
        v.edMerk.setText("")
    }
    val btnHapusKtgDialog = DialogInterface.OnClickListener{dialog,which->
        deleteDataKategori(idKategori)
        v.edMerk.setText("")
    }


}