package lana.ahmad.myapplication.uts

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpenHelper(context: Context):SQLiteOpenHelper(context, DB_Name,null, DB_Ver) {
    override fun onCreate(db: SQLiteDatabase?) {
        val tBarang ="create table barang(id_barang integer primary key autoincrement, nama_barang text not null)"
        val tKategori ="create table kategori(id_kategori integer primary key autoincrement, nama_kategori text not null)"
        val tStok = "create table stok(id_stok integer primary key autoincrement,nama_barang text not null, nama_kategori text not null,stok text not null)"
        db?.execSQL(tBarang)
        db?.execSQL(tKategori)
        db?.execSQL(tStok)
    }

    override fun onUpgrade(p0: SQLiteDatabase?, p1: Int, p2: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object{
        val DB_Name = "toko_bangunan"
        val DB_Ver = 1
    }
}