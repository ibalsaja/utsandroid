package lana.ahmad.myapplication.uts

import android.app.AlertDialog
import android.content.ContentValues
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.CursorAdapter
import android.widget.ListAdapter
import android.widget.SimpleCursorAdapter
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_data_brg.view.*


class FragmentBarang : Fragment(),View.OnClickListener {

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnTambahMerk->{
                builder.setTitle("Konfirmasi").setMessage("Data yang dimasukkan sudah benar?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnTambahMerkDialog)
                    .setNegativeButton("Tidak",null)
                builder.show()
            }
            R.id.btnEditMerk->{
                builder.setTitle("Konfirmasi").setMessage("Data yang diubah sudah benar?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnEditMerkDialog)
                    .setNegativeButton("Tidak",null)
                builder.show()
            }
            R.id.btnHapusMerk->{
                builder.setTitle("Konfirmasi").setMessage("Yakin ingin menghapus data ini?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnHapusMerkDialog)
                    .setNegativeButton("Tidak",null)
                builder.show()
            }
        }
    }
    lateinit var  thisParent : MainActivity
    lateinit var db : SQLiteDatabase
    lateinit var adapter : ListAdapter
    lateinit var v :View
    lateinit var builder : AlertDialog.Builder
    var idBarang : String = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        thisParent = activity as MainActivity
        db = thisParent.getDbObject()
        v = inflater.inflate(R.layout.frag_data_brg,container,false)
        v.btnEditMerk.setOnClickListener(this)
        v.btnTambahMerk.setOnClickListener(this)
        v.btnHapusMerk.setOnClickListener(this)
        builder = AlertDialog.Builder(thisParent)
        v.lsMerk.setOnItemClickListener(itemClick)
        return v
    }
    fun showDataBarang(){
        val cursor : Cursor=db.query("barang", arrayOf("nama_barang","id_barang as _id"),
            null,null,null,null,"nama_barang asc")
        adapter = SimpleCursorAdapter(thisParent,R.layout.item_data_brg,cursor, arrayOf("_id","nama_barang")
            ,intArrayOf(R.id.txIdMerk,R.id.txNamaMerk),CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        v.lsMerk.adapter = adapter
    }

    override fun onStart() {
        super.onStart()
        showDataBarang()
    }
    val itemClick = AdapterView.OnItemClickListener{parent,view,position,id->
        val c:Cursor=parent.adapter.getItem(position)as Cursor
        idBarang=c.getString(c.getColumnIndex("_id"))
        v.edMerk.setText(c.getString(c.getColumnIndex("nama_barang")))

    }
    fun insertDataBarang(namaBarang: String){
        var cv : ContentValues = ContentValues()
        cv.put("nama_barang",namaBarang)
        db.insert("barang",null,cv)
        showDataBarang()
    }
    fun updateDataBarang(namaBarang: String, idBarang: String){
        var cv : ContentValues = ContentValues()
        cv.put("nama_barang",namaBarang)
        db.update("barang",cv,"id_barang=$idBarang",null)
        showDataBarang()
    }
    fun deleteDataBarang(idBarang: String){
        db.delete("barang","id_barang=$idBarang",null)
        showDataBarang()
    }
    val btnTambahMerkDialog = DialogInterface.OnClickListener{dialog,which->
        insertDataBarang(v.edMerk.text.toString())
        v.edMerk.setText("")
    }
    val btnEditMerkDialog = DialogInterface.OnClickListener {dialog,which->
        updateDataBarang(v.edMerk.text.toString(),idBarang)
        v.edMerk.setText("")
    }
    val btnHapusMerkDialog = DialogInterface.OnClickListener{dialog,which->
        deleteDataBarang(idBarang)
        v.edMerk.setText("")
    }


}