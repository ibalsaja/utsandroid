package lana.ahmad.myapplication.uts

import android.app.AlertDialog
import android.content.ContentValues
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.CursorAdapter
import android.widget.ListAdapter
import android.widget.SimpleCursorAdapter
import androidx.fragment.app.Fragment

import kotlinx.android.synthetic.main.frag_data_stok.*
import kotlinx.android.synthetic.main.frag_data_stok.view.*

class FragmentStok : Fragment(), View.OnClickListener, AdapterView.OnItemSelectedListener {

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnTambahStok->{
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah data yang akan dimasukkan sudah benar?")
                    .setPositiveButton("Ya",btnInsertDialog)
                    .setNegativeButton("Tidak",null)
                dialog.show()
            }
//            R.id.btnEditStok->{
//                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
//                    .setMessage("Apakah data yang akan dimasukkan sudah benar?")
//                    .setPositiveButton("Ya",btnUpdateStokDialog)
//                    .setNegativeButton("Tidak",null)
//                dialog.show()
//            }
//            R.id.btnHapusStok->{
//                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
//                    .setMessage("Apakah data yang akan dimasukkan sudah benar?")
//                    .setPositiveButton("Ya",btnDeleteStokDialog)
//                    .setNegativeButton("Tidak",null)
//                dialog.show()
//            }
        }
    }

    lateinit var thisParent : MainActivity
    lateinit var v : View
    lateinit var lsAdapter : ListAdapter
    lateinit var spAdapter : SimpleCursorAdapter
    lateinit var spAdapter1 : SimpleCursorAdapter
    lateinit var dialog : AlertDialog.Builder
    lateinit var db : SQLiteDatabase
    var namaMerk : String = ""
    var namaKtg : String = ""


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.frag_data_stok,container,false)
        db = thisParent.getDbObject()
        dialog = AlertDialog.Builder(thisParent)
        v.btnHapusStok.setOnClickListener(this)
        v.btnTambahStok.setOnClickListener(this)
        v.btnEditStok.setOnClickListener(this)
        v.spMerk.onItemSelectedListener = this
        v.spKtg.onItemSelectedListener = this
        return v
    }
    override fun onStart() {
        super.onStart()
        showDataStok()
        showDataMerk()
        showDataKtg()
    }

    fun showDataStok(){
        var sql= "select n.id_stok as _id, m.nama_barang, k.nama_kategori, n.stok from stok n join barang m on n.nama_barang = m.nama_barang join kategori k on n.nama_kategori = k.nama_kategori order by n.id_stok asc"
        val c : Cursor = db.rawQuery(sql,null)
        lsAdapter = SimpleCursorAdapter(thisParent,R.layout.item_data_stok,c,
            arrayOf("_id","nama_barang","nama_kategori","stok"), intArrayOf(R.id.txIdStok,R.id.txMerk,R.id.txKtg,R.id.txStok),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        v.lsStok.adapter = lsAdapter
    }

    fun showDataMerk(){
        val c : Cursor = db.rawQuery("select nama_barang as _id from barang order by nama_barang asc",null)
        spAdapter1 = SimpleCursorAdapter(thisParent, android.R.layout.simple_spinner_item,c,
            arrayOf("_id"), intArrayOf(android.R.id.text1),CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        spAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        v.spMerk.adapter = spAdapter1
        v.spMerk.setSelection(0)
    }
    fun showDataKtg(){
        val c : Cursor = db.rawQuery("select nama_kategori as _id from kategori order by nama_kategori asc",null)
        spAdapter = SimpleCursorAdapter(thisParent, android.R.layout.simple_spinner_item,c,
            arrayOf("_id"), intArrayOf(android.R.id.text1),CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        spAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        v.spKtg.adapter = spAdapter
        v.spKtg.setSelection(0)
    }



    override fun onNothingSelected(parent: AdapterView<*>?) {
        spMerk.setSelection(0,true)
        spKtg.setSelection(0,true)
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val c = spAdapter1.getItem(position) as Cursor
        val c1 = spAdapter.getItem(position) as Cursor
        namaMerk = c1.getString(c.getColumnIndex("_id"))
        namaKtg = c.getString(c.getColumnIndex("_id"))
    }

    fun insertDataStok(nama_barang : String,nama_kategori : String, stok : String){
        var sql = "insert into stok(nim, kode, nilai) values (?,?,?)"
        db.execSQL(sql, arrayOf(nama_barang,nama_kategori,stok))
        showDataStok()
    }
    fun updateDataStok(
        nama_barang: String,
        nama_kategori: String,
        stok: String,
        idStok: String
    ) {
        var cv : ContentValues = ContentValues()
        cv.put("nama_barang",nama_barang)
        cv.put("nama_kategori",nama_kategori)
        cv.put("stok",stok)
        db.update("stok",cv,"id_stok = $idStok}",null)
        showDataStok()
    }
    fun deleteDataStok(idStok: String) {
        db.delete("stok","id_stok = $idStok",null)
        showDataStok()
    }

    val btnInsertDialog = DialogInterface.OnClickListener { dialog, which ->
        var sql = "select id_barang from barang where nama_barang='$namaMerk'"
        var sql1 = "select id_kategori from kategori where nama_kategori='$namaKtg'"
        var c : Cursor = db.rawQuery(sql,null)
        var c1 : Cursor = db.rawQuery(sql1,null)
        if(c.count>0 && c1.count>0){
            c.moveToFirst()
            c1.moveToFirst()
            insertDataStok(c.getString(c.getColumnIndex("id_barang")),c1.getString(c1.getColumnIndex("id_ktg")),v.edStok.text.toString())
            v.edStok.setText("")

        }
        var x = v.edStok.text.toString()
        v.txV.setText("$namaMerk, $namaKtg,$x")
        v.edStok.setText("")
    }
//    val btnUpdateStokDialog = DialogInterface.OnClickListener { dialog, which ->
//        var sqlMerk = "select nama_barang from barang where nama_barang = '$namaMerk'"
//        var sqlKategori = "select nama_kategori from kategori where nama_kategori = '$namaKtg'"
//        val c : Cursor = db.rawQuery(sqlMerk,null)
//        val c1 : Cursor = db.rawQuery(sqlKategori,null)
//        if(c.count>0 && c1.count>0){
//            c.moveToFirst()
//            c1.moveToFirst()
//            updateDataStok(
//                c.getString(c.getColumnIndex("id_bareng")),
//                c1.getString(c1.getColumnIndex("id_kategori")),
//                v.edStok.text.toString()
//            idStok
//            )
//            v.spMerk.setSelection(0)
//            v.spKtg.setSelection(0)
//            v.edStok.setText("")
//        }
   // }
//    val btnDeleteStokDialog = DialogInterface.OnClickListener { dialog, which ->
//        deleteDataStok(idStok )
//        v.spMerk.setSelection(0)
//        v.spKtg.setSelection(0)
//        v.edStok.setText("")
//    }
}